# python3 server.py 127.0.0.1 5555 cancion.mp3  d

import sys
import socketserver


IP = sys.argv[1]
PORT = sys.argv[2]
FICHERO = sys.argv[3]

class EchoHandler(socketserver.BaseRequestHandler):

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        print(f"Received: {received}")
        sock.sendto(crearRespuesta(received).encode('utf-8'), self.client_address)

def crearRespuesta(data):
    global respuesta
    metodos = ("INVITE", "BYE", "ACK")
    bloques = data.split()
    print(bloques[0])
    if len(bloques) != 3:
        respuesta = "SIP/2.0 400 Bad Request"
    elif bloques[0] not in metodos:
        respuesta = "SIP/2.0 405 Method Not Allowed"
    else:
        respuesta = "SIP/2.0 200 OK" 
    return respuesta


def checkError():
    if len(sys.argv) != 4:
        print("Usage: python3 server.py <IP> <port> <audio_file>")
    else:
        print("Listening...")


def main():
    checkError()
    try:
        serv = socketserver.UDPServer(('', int(PORT)), EchoHandler)
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)



if __name__ == "__main__":
    main()