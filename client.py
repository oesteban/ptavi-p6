# python3 client.py INVITE batman@193.147.73.20:5555
# python3 client.py BYE batman@193.147.73.20:5555


import sys
import socket

METODO = sys.argv[1]
DIRECCION = sys.argv[2]

def checkError():
    metodos = ("INVITE", "BYE", "ACK")
    if len(sys.argv) != 3:
        print("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
        sys.exit()
    elif METODO not in metodos:
        print("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
        sys.exit()

def dividir():
    global RECEPTOR
    global IPRECEPTOR 
    global PORTSIP
    bloque1 = DIRECCION.split('@') 
    bloque2 = bloque1[1].split(':')  
    RECEPTOR = bloque1[0]
    IPRECEPTOR = bloque2[0]
    PORTSIP = bloque2[1]


def crearPeticion():
    peticion = f"{METODO} sip:{RECEPTOR}@{IPRECEPTOR} SIP/2.0"
    return peticion
    



def main():
    checkError()
    dividir()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    
            LINE = crearPeticion()
            print(LINE)
            my_socket.sendto(LINE.encode('utf-8'), (IPRECEPTOR, int(PORTSIP)))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
           
    except ConnectionRefusedError:
        print("Error conectando a servidor")



if __name__ == "__main__":
    main()